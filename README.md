# GitPusher

A source control productivity booster. This program stores your current development branch and remote, both committing and pushing all changes with a single argument input. This is ideal for rapid commits and pushing when the development becomes intense.

**Note:** this program is in beta, bugs are to be expected until full release.

## Getting Started

Compling yourself: Use Visual Studio and the project file to create either a new project to be modified or add to a solution for use as a DLL.

### Requires:

Git

This project is target for .NET core 2.1. It should be compliant with earlier versions of course, however when compiling with VS, one will have to change the project settings.

Currently it is only developed for Windows but linux support is eyed for the future. See the [roadmap](#Roadmap) section below.

### Built With

Since **Beta 2.0.3** the project has been built with a very simple INI parser library of my own design.

* [INIParser](https://bitbucket.org/uaineteinestudio/iniparser) by [UaineTeine](https://bitbucket.org/uaineteinestudio/) - The INI file parser library which is required to compile the software.

**Beta 1.0.3** and earlier versions required an Ini-Parser package:

* [Ini-Parser](https://github.com/rickyah/ini-parser) by [rickyah](https://github.com/rickyah) - The INI file parser library which is required to compile the software. [GitHub](https://github.com/rickyah/ini-parser) |  [NuGet](https://www.nuget.org/packages/ini-parser/)

### Installing

##### Building with Windows:

Use Visual Studio to build the project file into an executable, deploy to the working directory of any future project you are making.

##### Building with Linux:

Open terminal/cmd and use .NET core to build with:

```
dotnet publish -c release -r ubuntu.16.04-x64 --self-contained
```

For either platform the built app into your working directory. Git creditional storing is required in that project folder. Set with:
```
git config credential.helper store
```

## Authors

* **Daniel Stamer-Squair** - *UaineTeine*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Roadmap

**Beta 2.1**

* Complete linux use
* GUI app